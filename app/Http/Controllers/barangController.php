<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\barang;

class barangController extends Controller
{
    //show all list of items
    public function index(){
        $allBarang = barang::all();
        return $allBarang->toJson();
    }

    public function show($id){
        $oneBarang = barang::find($id);

        return $oneBarang->toJson();
    }

    public function search($keyword){
        $searchBarang = barang::where('titleBarang','LIKE','%'.$keyword.'%')->get();
        return $searchBarang->toJson();
    }
}
