<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Chating extends Model
{
    protected $fillable = [
        'user_id',
        'textuser',
        'textadmin',
    ];

    protected $casts = [
        'user_id' => 'integer',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
