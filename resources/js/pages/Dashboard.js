import React, { Component } from 'react';
import styled from 'styled-components';
import Product from '../pages/body/Product';
import '../App.css';
import Header from '../components/Header';
import Footer from '../components/Footer';
class Dashboard extends Component {
  constructor() {
    super();

    this.state = {
      afterLoadText: 'afterLoad triggered',
      beforeLoadText: 'beforeLoad triggered',
      showLowResImages: true,
      threshold: 100
    };
  }


  render() {
    const {afterLoadText, beforeLoadText, direction,
      effect, showLowResImages, threshold, containerWithOverflow} = this.state;
    return (
      <div>
      <Header/>
          <div style={{paddingBottom:"50px"}}></div>
              <ProductWrapper>
                  <Product
                    afterLoadText={afterLoadText}
                    beforeLoadText={beforeLoadText}
                    showLowResImages={showLowResImages}
                    threshold={threshold}
                  />
              </ProductWrapper>
          <div style={{paddingBottom:"60px"}}></div>
      <Footer/>
      </div>
    );
  }
}


export default Dashboard;

const ProductWrapper = styled.div`
div{
    display: inline-block;
}
`;