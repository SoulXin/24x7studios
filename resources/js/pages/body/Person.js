import React, { Component } from 'react';
import * as actions from '../../store/actions';
import { connect } from 'react-redux';



class Person extends Component {

    constructor() {
        super();
        this.state = {
            credential:{},
            email : "",
            password : ""
        };
    }

    handleLogout = (e) => {
        e.preventDefault();
        this.props.dispatch(actions.authLogout());
    }

    render() {
        return (
            <div>
                {/*Header*/}
                <div style = {{backgroundColor:"#2ED1A2"}}>
                    <table>
                        <tbody>
                            <tr>
                                <td><img style={{borderRadius:"100px"}} src = "icon/acount.png" width="100px" /></td>
                                <td>{this.props.userName}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                {/*Body*/}
                <div>
                    <table>
                        <tbody>
                            <tr>
                                <td>03</td>
                                <td>February</td>
                                <td>1999</td>
                            </tr>
                        </tbody>
                    </table>

                    <hr/>

                    <p>Jenis Kelamin</p>
                    <p>Laki-Laki</p>

                    <hr/>

                    <p>Provinsi</p>
                    <p>Sumatera Utara</p>
                </div>

                {/*Footer*/}
                <div style={{textAlign:"center"}}>
                    <button onClick = {this.handleLogout} style = {{backgroundColor:"white",paddingRight:"0px",paddingLeft:"0px",width:"100%",height:"40px",borderRadius:"10px",bottom:"0",left:"0",position:"fixed"}}>
                        Logout
                    </button>
                </div>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.Auth.isAuthenticated,
    userName : state.Auth.user.name
  });

export default connect(mapStateToProps)(Person);
