import React, { Component } from 'react'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import Http from '../../Http';

class Cart extends Component {
  constructor(props) {
    super(props);

    // Initial state.
    this.state = {
      todo: null,
      error: false,
      emptycart:"",
      data: [],
      total:[],

    };

    // API endpoint.
    this.api = '/api/v1/todo';
  }

  componentDidMount() {
    var a;
    var bill = 0;
    Http.get(`${this.api}?status=open`)
      .then((response) => {
        const { data } = response.data;
        console.log(data);
        if(data == 0)
        {
          this.setState({
            emptycart : "Keranjang anda masih kosong bos"
          })
        }
        this.setState({
          data, error: false,
        });

        for(a = 0;a<=data.length;a++){
          bill += data[a].price;
          this.setState({
            total : bill
          })
        }

      })
      .catch(() => {
        this.setState({
          error: 'Unable to fetch data.',
        });
      });
  }

  closeTodo = (e) => {
    const { key } = e.target.dataset;
    const { data: todos } = this.state;
    var a;
    var bill = 0;

    Http.patch(`${this.api}/${key}`, { status: 'closed' })
      .then(() => {
        const updatedTodos = todos.filter(todo => todo.id !== parseInt(key, 10));
        this.setState({ data: updatedTodos,
         });
      })
      .catch(() => {
        this.setState({
          error: 'Sorry, there was an error closing your to do.',
        });
      });

      Http.get(`${this.api}?status=open`)
      .then((response) => {
        const { data } = response.data;
        console.log(data.length);
        for(a = 0;a<data.length;a++){
          bill += data[a].price;
          this.setState({
            total : bill
          })
        }
    })
    .catch(() => {
      this.setState({
        error: 'Unable to fetch data.',
      });
    });
  }


  render() {
    const {
      data, error,total,emptycart
    } = this.state;

    return (
      <div>

        <div style={{backgroundColor:"#2ED1A2"}}>
          <table>
            <tbody>
            <tr>
              <td><Link to = "/"><img src = "icon/back.png" width="40px"/></Link></td>
              <td>Keranjang Belanja</td>
            </tr>
            </tbody>
          </table>
        </div>

        {data.length>0 ? (
        <div>
          <table className="table table-striped" style={{marginBottom:"40px"}}>
            <tbody>
              {data.map(todo => (
                <tr key={todo.id}>
                  <td>
                    <img src={todo.image} style={{width:"70px",height:"70px"}}/>
                  </td>
                  <td>
                    <small style={{color:"#e74c3c"}}><i>{todo.value}</i></small>
                    <br/>
                    <p style={{color:"#2ED1A2"}}>Rp.{todo.price}</p>
                  </td>
                  
                  <td>
                  <button
                      type="button"
                      className="btn"
                      style={{backgroundColor:"#e74c3c"}}
                      onClick={this.closeTodo}
                      data-key={todo.id}
                    >
                    Hapus
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        

          <footer className="d-flex align-items-center justify-content-between">
            <div style = {{backgroundColor:"#2ED1A2",position:"fixed",bottom:"0",width:"100%",height:"40px"}}>
              <p>Total Harga : <i>Rp.</i><span style={{fontSize:"25px"}}>{data.length>0? total : ""}</span></p>
              <button type="submit" style={{border:"none",backgroundColor:"#e74c3c",borderRadius:"5px",position:"fixed",bottom:"1.3%",right:"3%"}}>Bayar</button> 
            </div>
          </footer>
        </div>
      ):
      (
        <div style={{textAlign:"center"}}>
          <p>Keranjang anda masih kosong bos</p>
          <Link style={{textDecoration:"none",color:"black",backgroundColor:"#e74c3c",borderRadius:"5px",padding:"10px"}} to = "/">Kembali berbelanja</Link>
        </div>
      )}
       
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.Auth.isAuthenticated,
  user: state.Auth.user,
});

export default connect(mapStateToProps)(Cart);
