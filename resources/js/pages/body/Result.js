import React, { Component } from 'react';
import axios from 'axios';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import MediaQuery from 'react-responsive';

class Result extends Component {

    constructor(props){
        super(props);
        this.state={
            result:[],
            title:[],
            textinput:'',
        }
        this.TextChange = this.TextChange.bind(this);
    }
  
    
    FormSearch(event){
        event.preventDefault();
    }

    TextChange(event){
        this.setState({
            textinput:event.target.value
        })
        
    }

    componentDidMount(){
        const projectTitle = this.props.match.params.titleBarang;
        axios.get(`http://127.0.0.1:8000/api/search/${projectTitle}`).then(
            response=>{
                console.log(projectTitle);
                console.log(response);
                this.setState({
                    result:response.data,
                    title:projectTitle
                });
            }
        ).catch(function(error){
            console.log(error)
        })
    }

    imageSplit(string,number){
        var stringsplit = string; 
        var array = new Array();
        array = stringsplit.split("|");
        
        return array[number];
    }

    SearchBarEnter  = event => {
        if (event.key == 'Enter') {
            document.getElementById("searchbottom").click();
            document.getElementById("inputbox").blur();
            document.getElementById("inputbox").value = "";
            const projectTitle = this.state.textinput;
            axios.get(`http://127.0.0.1:8000/api/search/${projectTitle}`).then(
                response=>{
                    console.log(projectTitle);
                    console.log(response);
                    this.setState({
                        result:response.data,
                        title:projectTitle
                    });
                }
            ).catch(function(error){
                console.log(error)
            })
            console.log(this.state.textinput);
            
        }
    };

    render() {
        return (
            <div>
                    <NavWrapper className = "navbar navbar-expand-sm bg-primary navbar-dark px-sm-5">
                        <div className="inti">
                            <div className="backicon">
                                <Link to ="/">
                                    <img src="https://cdn0.iconfinder.com/data/icons/typicons-2/24/arrow-back-512.png" width="32px"></img>
                                </Link>
                            </div>
                            
                            <div className="inputbar">
                            <form onSubmit={this.FormSearch}>
                                <input id="inputbox" onChange={this.TextChange} onKeyUp={this.SearchBarEnter} type="text" placeholder={this.state.title} style={{width:"100%"}} />
                            </form>
                            <Link to={`/result/${this.state.textinput}`} id="searchbottom" ></Link>
                            </div>
                        </div>
                    </NavWrapper>
            <div style={{paddingTop:"32.5px"}}></div>
            {
                this.state.result.map(listresult=>{
                    return(
                        <MediaQuery minWidth={310} maxWidth={320} key={listresult.id}>
                            <ProductWrapper310320 className = "m-1" style={{display:"inline-block"}}>
                                <Link to = {`/details/${listresult.id}`}>
                                <div className = "card">
                                {/*Image*/}
                                    <div className="img-container">
                                        <img  src = {this.imageSplit(listresult.imgSrc,[0])} alt = "Product" style={{height:"170px"}} className = "card-img-top"></img>
                                    </div>

                                {/* Card Footer*/}
                                    <div className = "harga" style = {{paddingLeft:"10px",paddingRight:"10px"}}>
                                        <small style={{color:"red"}}>{listresult.titleBarang}</small><br/>
                                        <small style={{color:"lime"}}>Rp.{listresult.harga}</small>
                                    </div>
                                </div>
                                </Link>
                            </ProductWrapper310320>
                        </MediaQuery> 
                    )
                })
            }
        </div>
        );
    }
}

export default Result;

const ProductWrapper310320 = styled.div`
.card{
border-color : transparent;
transition : all 1s linear;
width 150px;
border: 1px solid grey;
box-shadow : 1px 1px 2px 0px rgba(0,0,0,0.2);
border-radius:5px;
}
.harga{
border-radius:0px 0px 10px 10px;
color:lime;
padding:0px;
}
.img-container{
position : relative;
overflow : hidden;
border-radius:5px;
}
.card-img-top{
transition : all 1s linear;
}
`;

const NavWrapper = styled.div`
.inti{
    display: flex;
    flex-direction: row;
    padding:8px;
    background-color:var(--ToscaGreen)!important;
    top:0;
    left:0;
    width:100%;
    position:fixed;
    z-index:1;
}

.backicon{
    flex: 0.15;  
}

.inputbar{
    flex: 1;
}
`;
