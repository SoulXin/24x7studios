import React, { Component } from 'react';
import axios from 'axios';
import Modal from 'react-awesome-modal';
import MediaQuery from 'react-responsive';
import Slider from "react-slick";
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import Http from '../../Http';

class Details extends Component {
    constructor(props){
        super(props);
        this.state={
            item:[],
            image:[],
            imagecart:[],
            visible : false,
            todo: null,
            error: false,
            data: [],
        };
            // API endpoint.
    this.api = '/api/v1/todo';
    }

    addTodo = () => {
        Http.post(this.api, {value:this.state.item.titleBarang,image:this.state.image[0],price:this.state.item.harga})
          .then(({ data }) => {
            const newItem = {
                id: data.id,
                value: this.state.item.titleBarang,
                image : this.state.image[0],
                price : this.state.item.harga,
            };
            console.log(newItem);

            const allTodos = [newItem, ...this.state.data];
            this.setState({ data: allTodos, todo: null });
            this.todoForm.reset();
          })
          .catch(() => {
            this.setState({
              error: 'Sorry, there was an error saving your to do.',
            });
          });
      }

    openModal() {
        this.setState({
            visible : true
        });
    }

    closeModal() {
        this.setState({
            visible : false
        });
    }

    componentDidMount(){
        const projectId = this.props.match.params.id;
        axios.get(`http://127.0.0.1:8000/api/barang/${projectId}`).then(
            response=>{
                console.log(response);
                console.log(projectId);
                this.setState({
                    item:response.data,
                    image:response.data.imgSrc.split("|")
                });
            }
        ).catch(function(error){
            console.log(error)
        })
    }


    render() {
        var settings = {
            dots: true,
            infinite: true,
            speed: 200,
            slidesToShow: 1,
            slidesToScroll: 1,
            accessibility:false,
            arrows:false
            };
        const {item} = this.state;
        const  {image} = this.state;
        return (
            <div> 
                <MediaQuery minWidth={316} maxWidth={320}>
                <div style={{backgroundColor:"#2ED1A2"}}>
                    <table>
                        <tbody>
                            <tr>
                                <td><Link to = "/"><img src = "../icon/back.png" width="40px"></img></Link></td>
                                <td>Detail Produk</td>
                                <td style={{paddingLeft:"140px"}}><Link to = "/cart"><img  src = "../icon/basket.png" width="30px"/></Link></td>
                            </tr>
                        </tbody>
                    </table> 
                </div>
                <div>
                    <Slider {...settings}>
                        <div onDoubleClick={() => this.openModal()} >
                        <img src={image[0]} style={{width:"100%",height:"300px"}}/>
                        </div>
                        <div onDoubleClick={() => this.openModal()}>
                        <img src={image[1]} style={{width:"100%",height:"300px"}}/>
                        </div>
                        <div onDoubleClick={() => this.openModal()}>
                        <img src={image[2]} style={{width:"100%",height:"300px"}}/>
                        </div>
                    </Slider>
                </div>
                <br/>
                    {/* Judul + Harga Produk*/}
                    <div style={{borderRadius:"5px",backgroundColor:"#F0F0F0",boxShadow:"1px 1px 3px 1px #D3D3D3"}}>
                        <small style={{paddingLeft:"10px",color:"red",fontSize:"20px"}}>{item.titleBarang}</small><br/>
                        <small style={{paddingLeft:"10px",color:"lime",fontSize:"25px"}}>Rp.{item.harga}</small>
                    </div>
                    <br/>
                    {/*Deskripsi Produk*/}
                    <div style={{borderRadius:"5px",backgroundColor:"#F0F0F0",boxShadow:"1px 1px 3px 1px #D3D3D3"}}>
                        <p style={{paddingLeft:"10px"}}>{item.deskripsi}</p>
                    </div>
                    
                    <div style={{bottom:"0",position:"fixed",display:"flex",width:"100%"}}>

                            <button style = {{width:"100%",border:"none",backgroundColor:"#e74c3c",padding:"10px"}} onClick={this.addTodo}  >Tambah Keranjang</button>
                            <Link  style = {{width:"100%",border:"none",backgroundColor:"#2ED1A2",padding:"10px",textAlign:"center",color:"black",textDecoration:"none"}} onClick={this.addTodo} to = "/cart">Beli Sekarang</Link>
                    </div>
                        <Modal visible={this.state.visible} width="100%" height="100%" effect="fadeInUp" onClickAway={() => this.closeModal()}>
                            <div style={{backgroundColor:"black",paddingTop:"10%",height:"100%"}}>
                                <a href="javascript:void(0);" onClick={() => this.closeModal()} style={{marginLeft:"250px",fontSize:"20px"}}>Close</a>
                                <br/>
                                <br/>
                                <Slider {...settings}>
                                    <div>
                                    <img src={image[0]} width="100%" height="350px"/>
                                    </div>
                                    <div>
                                    <img src={image[1]} width="100%" height="350px"/>
                                    </div>
                                    <div>
                                    <img src={image[2]} width="100%" height="350px"/>
                                    </div>
                                </Slider>
                            </div>
                        </Modal>
                </MediaQuery>
            </div>
                
        );
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.Auth.isAuthenticated,
    user: state.Auth.user,
  });

export default connect(mapStateToProps)(Details);