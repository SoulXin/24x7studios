import React, { Component } from 'react';
import axios from 'axios';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import MediaQuery from 'react-responsive';
import {LazyLoadImage} from 'react-lazy-load-image-component';

class Product extends Component {

    constructor(props){
        super(props);
        this.state={
            barang:[],
            tampil:""

        }
    }

    componentDidMount(){
        axios.get('http://127.0.0.1:8000/api/barang').then(
            response=>{
                console.log(response);
                console.log(response.data[1].titleBarang);
                this.setState({barang:response.data});
            }
        ).catch(function(error){
            console.log(error)
        })
        this.setState({
            tampil : 1
        })
    }

    imageSplit(string,number){
        var stringsplit = string; 
        var array = new Array();
        array = stringsplit.split("|");
        
        return array[number];
    }

    countText(text){
        var totaltext = text.length;
        var showtext;
        if(totaltext <= 50){
            showtext = text.substring(0,50);
            return showtext;
        }else if(totaltext >= 51){
            showtext = text.substring(0,50) + " ...";
            return showtext;
        }
    }
    render() {
        const {afterLoadText, beforeLoadText, effect, direction,
            scrollPosition, showLowResImages, threshold, containerWithOverflow
          } = this.props;
          
        return (
        <div>
            {
                this.state.barang.map(listbarang=>{
                    return(
                        <MediaQuery minWidth={316} maxWidth={320} key={listbarang.id}>
                            <ProductWrapper316320 className = "m-1">
                                <Link to = {`/details/${listbarang.id}`} href={`/details/${listbarang.id}`}>
                                <div className = "card">
                                {/*Image*/}
                                    <LazyLoadImage style={{height:"150px",width:"150px",borderRadius:"5px",zIndex:1}} 
                                            alt="Product"
                                            key={this.imageSplit(listbarang.imgSrc,[0])}
                                            placeholderSrc={showLowResImages ?  "icon/Loading.gif" : null}
                                            src={this.imageSplit(listbarang.imgSrc,[0])}
                                            threshold={threshold}
                                            afterLoad={() => console.log(afterLoadText)}
                                            beforeLoad={() => console.log(beforeLoadText)}
                                    />
                                {/* Card Footer*/}
                                
                                    <div className = "harga" style = {{paddingLeft:"10px",paddingRight:"10px"}}>
                                        <small style={{color:"red"}}>{listbarang.titleBarang}</small><br/>
                                        <small style={{color:"lime"}}>Rp.{listbarang.harga}</small>
                                    </div>
                                </div>
                                </Link>
                                
                            </ProductWrapper316320>
                        </MediaQuery> 
                    )
                })
            }
        </div>
        );
    }
}

export default Product;

const ProductWrapper316320 = styled.div`
.card{
border-color : transparent;
transition : all 1s linear;
width 150px;
border: 1px solid grey;
box-shadow : 1px 1px 2px 0px rgba(0,0,0,0.2);
border-radius:5px;
}
.harga{
border-radius:0px 0px 10px 10px;
color:lime;
padding:0px;
}
.img-container{
position : relative;
overflow : hidden;
border-radius:5px;
}
.card-img-top{
transition : all 1s linear;
}
`;
{/*<h1 key={listbarang.id}>{listbarang.titleBarang}</h1>*/}
