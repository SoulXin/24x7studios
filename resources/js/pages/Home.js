import React, { Component } from 'react';
import styled from 'styled-components';
import Product from '../pages/body/Product';
import '../App.css';
import Header from '../components/Header';
import Footer from '../components/Footer';
class ProductList extends Component {


    render() {
        return (
            <div>
            <Header/>
                <div style={{paddingBottom:"50px"}}></div>
                    <ProductWrapper>
                        <Product/>
                    </ProductWrapper>    
                        
            <Footer/>
            </div>
        );
    }
}

export default ProductList;
const ProductWrapper = styled.div`
div{
    display: inline-block;
}
`;