import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';
import styled from 'styled-components';
class Footer extends Component {
    
/* 
  handleLogout = (e) => {
    e.preventDefault();
    this.props.dispatch(actions.authLogout());
  }
*/

    GoToTop(){
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    componentDidMount(){
        axios.get('http://127.0.0.1:8000/api/barang').then(
            response=>{
                console.log(response.data.length);
                if(response.data.length < 4){
                    console.log("ada");
                    document.getElementById("footermenu").style.position = "fixed";
                    document.getElementById("footermenu").style.bottom = "0";
                    document.getElementById("footermenu").style.width = "100%";
                }else if(response.data.length > 4){
                    document.getElementById("footermenu").style.position = "absolute";
                    document.getElementById("footermenu").style.marginTop = "50px";
                    document.getElementById("footermenu").style.width = "100%";
                }
            }
        ).catch(function(error){
            console.log(error)
        })
        this.setState({
            tampil : 1
        })
    }
    
  render() {
    return (
      <footer className="d-flex align-items-center justify-content-between">
      {!this.props.isAuthenticated &&
            <NavWrapper  id="footermenu">
                <div className="inti">
                    <div className="homeicon" onClick={this.GoToTop}> 
                        <Link to = "/">
                            <img src="icon/home.png" width="32px"></img>
                        </Link>
                    </div>

                    <div className="categoryicon">
                        <Link to="/message">
                            <img src="icon/category.png" width="32px"></img>
                        </Link>
                    </div>

                    <div className="basketicon">
                        <Link to = "/cart">
                            <img src="icon/basket.png" width="32px"></img>
                        </Link>
                    </div>

                    <div className="acounticon">
                        <Link to="/login">
                            <img src="icon/acount.png" width="32px"></img>
                        </Link>
                    </div>
                </div>
            </NavWrapper>
        }
        {this.props.isAuthenticated &&
         
        <NavWrapper  id="footermenu">
                <div className="inti">
                    <div className="homeicon" onClick={this.GoToTop}> 
                        <Link to = "/">
                            <img src="icon/home.png" width="32px"></img>
                        </Link>
                    </div>

                    <div className="categoryicon">
                        <Link to="/message">
                            <img src="icon/category.png" width="32px"></img>
                        </Link>
                    </div>

                    <div className="basketicon">
                        <Link to = "/cart">
                            <img src="icon/basket.png" width="32px"></img>
                        </Link>
                    </div>

                    <div className="acounticon">
                        {/*
                        <button onClick = {this.handleLogout} style = {{border:"none",backgroundColor:"transparent",paddingRight:"0px",paddingLeft:"0px"}}>
                            <img src="icon/logout.png" width="32px"></img>
                        </button>
                        */}
                        
                        <Link to = "/person">
                            <img src="icon/acount.png" width="32px"></img>
                        </Link>
                    </div>
                </div>
            </NavWrapper>
                /*<Nav>
                    <NavItem>
                    <NavLink tag={Link} to="/archive">Archive</NavLink>
                    </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                        Account
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem>
                        Settings
                        </DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem onClick={this.handleLogout}>
                        Log Out
                        </DropdownItem>
                    </DropdownMenu>
                    </UncontrolledDropdown>
                </Nav>*/
        
        }

      </footer>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.Auth.isAuthenticated,
});

export default connect(mapStateToProps)(Footer);

const NavWrapper = styled.div`
.inti{
    display: flex;
    flex-direction: row;
    padding:8px;
    background-color:var(--ToscaGreen)!important;
    bottom:0;
    z-index:1;
}

.homeicon{
    flex: 2;
}

.categoryicon{
    flex: 2;
}

.basketicon{
    flex: 2;
}

.acounticon{
    float: 2;
}
`;
