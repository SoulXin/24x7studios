import React, { Component } from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';

export default class NavbarHome extends Component {

    constructor(props){
        super(props);
        this.state={
            textinput:'',
        }
        this.TextChange = this.TextChange.bind(this);
    } 

    FormSearch(event){
        event.preventDefault();
    }

    TextChange(event){
        this.setState({
            textinput:event.target.value
        })
    }

    SearchBarEnter  = event => {
        if (event.key == 'Enter') {
            document.getElementById("searchbottom").click();
        }
    };

    render() {
        return (
            <NavWrapper className = "navbar navbar-expand-sm bg-primary navbar-dark px-sm-5">
                <div className="inputbar" style={{width:"100%"}}>
                        <form onSubmit={this.FormSearch}>
                                <input onChange={this.TextChange} onKeyUp={this.SearchBarEnter} id="searchbox" type="text" placeholder="&#128269;  Cari di Kuylahbos" style={{width:"100%"}} />
                        </form>
                    </div>
                    <Link to={`/result/${this.state.textinput}`} id="searchbottom" style={{width:"100%"}}></Link>
            </NavWrapper>
        );
    }
}

const NavWrapper = styled.nav`
    background : var(--ToscaGreen)!important;
    .nav-link{
        color : var(--mainWhite)!important;
        font-size : 1.3rem;
        text-transform : capitalize;
    }
    padding:9px;
    position : fixed;
    width : 100%;
    top: 0;
    left:0;
    z-index: 1;
`;